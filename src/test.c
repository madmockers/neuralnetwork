
#include <stdio.h>
#include <stdlib.h>

#include <nn/activations.h>
#include <nn/status.h>
#include <nn/network.h>

#include <nn/layer/convolution.h>
#include <nn/layer/fullyconnected.h>
#include <nn/layer/pool.h>

static
NNStatus
    AddConvolutionPoolCombo
    (
        NeuralNetwork_t *Network,
        size_t          Count,
        unsigned int    Dimensions,
        size_t          *ConvolutionFieldSize,
        size_t          *PoolFieldSize
    )
{
    NNStatus status = NNSTATUS_OK;

    for( size_t idx = 0 ; idx < Count           &&
                          NNSTATUS_OK == status ; idx++ )
    {
        status = NeuralNetwork_AddLayer( Network,
                                         Activation_ReLU,
                                         LAYER_Convolution( Dimensions,
                                                            ConvolutionFieldSize ) );
        if( NNSTATUS_OK == status )
        {
            status = NeuralNetwork_AddLayer( Network,
                                             Activation_Passthrough,
                                             LAYER_Pool( Dimensions,
                                                         PoolFieldSize,
                                                         POOL_TYPE_MAX ) );
        }
    }
}

int
    main
    (

    )
{
    NNStatus status;

    size_t dims[] = { 1920, 1080 };

    NeuralNetwork_t *network;

    status = NeuralNetwork_New( 2, dims, &network );

    if( NNSTATUS_OK == status )
    {
        printf( "Created network\n" );

        size_t  fieldSize[] = { 5, 5 };
        size_t  poolSize[]  = { 2, 2 };

        status = AddConvolutionPoolCombo( network,
                                          6,
                                          2,
                                          fieldSize,
                                          poolSize );

        if( NNSTATUS_OK == status )
        {
            printf( "Added layer\n" );

            status = NeuralNetwork_AddLayer( network,
                                             Activation_Sigmoid,
                                             LAYER_FullyConnected( 9 ) );

            if( NNSTATUS_OK == status )
            {
                printf( "Added layer\n" );

                NeuralInstance_t *instance;

                status = NeuralNetwork_NewInstance( network, &instance );

                if( NNSTATUS_OK == status )
                {
                    printf( "Instance created\n" );
                    printf( "Weights: %zu\n", instance->WeightCount );
                    printf( "Neurons: %zu\n", instance->ValueCount );

                    double *inputs = calloc( dims[ 0 ] * dims[ 1 ], sizeof( double ) );
                    double outputs[ 9 ];

                    status = NeuralInstance_Evaluate( instance, inputs, outputs );

                    free( inputs );
                    free( instance );
                }
            }
        }

        NeuralNetwork_Free( network );
    }

    printf( "Status: %d\n", status );

    return 0;
}
