
#pragma once

#include <stdlib.h>

typedef double ( *ActivationFn )( double Input );

double
    Activation_Sigmoid
    (
        double      Value
    );

double
    Activation_ReLU
    (
        double      Value
    );

#define Activation_Passthrough ( NULL )
