
#pragma once

#include <stdlib.h>

#include <nn/layer.h>
#include <nn/status.h>

NNStatus
    LayerInit_FullyConnected
    (
        NeuronLayer_t   **pLayer,
        NeuralNetwork_t *Network,
        ActivationFn    Activate,
        va_list         Args
    );

#define LAYER_FullyConnected( OutputCount )     \
    LayerInit_FullyConnected,                   \
    ( size_t ) ( OutputCount )
