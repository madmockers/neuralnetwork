
#pragma once

#include <stdarg.h>
#include <stdlib.h>

#include <nn/layer.h>
#include <nn/status.h>

typedef enum
{
    POOL_TYPE_UNKNOWN   = 0,
    POOL_TYPE_MAX,
    POOL_TYPE_MIN,
} PoolType_t;

NNStatus
    LayerInit_Pool
    (
        NeuronLayer_t   **pLayer,
        NeuralNetwork_t *Network,
        ActivationFn    Activate,
        va_list         Args
    );

#define LAYER_Pool( DimensionCount,                     \
                    ReceptiveFieldDimensions,           \
                    PoolType )                          \
    LayerInit_Pool,                                     \
    ( unsigned int ) ( DimensionCount ),                \
    ( size_t * )     ( ReceptiveFieldDimensions ),      \
    ( PoolType_t )   ( PoolType )
