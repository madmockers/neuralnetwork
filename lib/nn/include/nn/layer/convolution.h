
#pragma once

#include <stdarg.h>
#include <stdlib.h>

#include <nn/layer.h>
#include <nn/status.h>

NNStatus
    LayerInit_Convolution
    (
        NeuronLayer_t   **pLayer,
        NeuralNetwork_t *Network,
        ActivationFn    Activate,
        va_list         Args
    );

#define LAYER_Convolution( DimensionCount,              \
                           ReceptiveFieldDimensions )   \
    LayerInit_Convolution,                              \
    ( unsigned int ) ( DimensionCount ),                \
    ( size_t * )     ( ReceptiveFieldDimensions )
