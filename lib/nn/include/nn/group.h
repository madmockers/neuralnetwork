
#pragma once

#include <nn/neuron.h>
#include <nn/status.h>

typedef struct NeuralNetwork  NeuralNetwork_t;
typedef struct NeuralInstance NeuralInstance_t;

typedef struct
{
    unsigned int    DimensionCount;
    size_t          *Dimensions;
    size_t          TotalCount;
    Neuron_t        *Neurons[ 0 ];
} NeuronGroup_t;

typedef
NNStatus
    ( *NeuronForEachFn )
    (
        void            *Ctxt,
        size_t          Idx,
        Neuron_t        *Neuron
    );

NNStatus
    NeuronGroup_Allocate
    (
        NeuralNetwork_t *Network,
        size_t          NeuronInputCount,
        unsigned int    DimensionCount,
        const size_t    *Dimensions,
        void            *InitCtxt,
        NeuronForEachFn InitFunction,
        NeuronGroup_t   **pAllocation
    );

void
    NeuronGroup_Free
    (
        NeuronGroup_t   *Group,
        void            *FiniCtxt,
        NeuronForEachFn FiniFunction
    );

NNStatus
    NeuronGroup_ForEach
    (
        NeuronGroup_t   *Group,
        void            *Ctxt,
        NeuronForEachFn ForEach
    );

NNStatus
    NeuronGroup_SetOutputs
    (
        NeuronGroup_t       *Group,
        NeuralInstance_t    *Instance,
        double              *Values
    );

NNStatus
    NeuronGroup_GetOutputs
    (
        NeuronGroup_t       *Group,
        NeuralInstance_t    *Instance,
        double              *Values
    );

NNStatus
    NeuronGroup_Evaluate
    (
        NeuronGroup_t       *Group,
        NeuralInstance_t    *Instance,
        NeuronLayer_t       *Layer
    );
