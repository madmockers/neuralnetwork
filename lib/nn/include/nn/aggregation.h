
#pragma once

typedef struct NeuralInstance   NeuralInstance_t;
typedef struct NeuronLayer      NeuronLayer_t;
typedef struct Neuron           Neuron_t;

typedef
double
    ( *AggregationFn )
    (
        NeuralInstance_t    *Instance,
        NeuronLayer_t       *Layer,
        Neuron_t            *Neurons,
        double              Initial
    );

#define Aggregate_Sum       ( NULL )
