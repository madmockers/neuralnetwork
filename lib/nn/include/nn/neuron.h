
#pragma once

#include <stdlib.h>

#include <nn/aggregation.h>
#include <nn/activations.h>

typedef struct Neuron
{
    AggregationFn   Aggregate;
    ActivationFn    Activate;

    size_t          InputCount;
    size_t          WeightsIdx;
    size_t          ValueIdx;

    struct Neuron   *Inputs[ 0 ];
} Neuron_t;
