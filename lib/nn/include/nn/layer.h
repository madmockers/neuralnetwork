
#pragma once

#include <stdarg.h>

#include <nn/activations.h>
#include <nn/list.h>
#include <nn/status.h>
#include <nn/group.h>

typedef struct NeuralNetwork NeuralNetwork_t;
typedef struct NeuronLayer   NeuronLayer_t;

typedef
NNStatus
    ( *NeuronLayerInitFn )
    (
        NeuronLayer_t           **pLayer,
        NeuralNetwork_t         *Network,
        ActivationFn            ActivateFn,
        va_list                 Args
    );

typedef
NNStatus
    ( *AllocateLayerFn )
    (
        NeuronLayer_t           *Layer,
        const NeuronGroup_t     *Inputs
    );

typedef
void
    ( *DeallocateLayerFn )
    (
        NeuronLayer_t           *Layer
    );

typedef
NNStatus
    ( *PreEvaluateFn )
    (
        NeuronLayer_t           *Layer
    );

typedef struct
{
    AllocateLayerFn             Allocate;
    DeallocateLayerFn           Deallocate;

    PreEvaluateFn               PreEvaluate;
} NeuronLayerItf_t;

struct NeuronLayer
{
    struct list_head        List;
    const NeuronLayerItf_t  *Itf;

    NeuralNetwork_t         *Network;

    size_t                  HiddenGroupCount;
    NeuronGroup_t           **HiddenGroups;

    NeuronGroup_t           *OutputNeurons;
};
