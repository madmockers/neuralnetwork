
#pragma once

#include <nn/activations.h>
#include <nn/status.h>
#include <nn/layer.h>
#include <nn/list.h>
#include <nn/neuron.h>

typedef struct NeuralNetwork NeuralNetwork_t;

typedef struct NeuralInstance
{
    NeuralNetwork_t     *Network;

    size_t              WeightCount;
    double              *Weights;

    size_t              ValueCount;
    double              *Values;
} NeuralInstance_t;

NNStatus
    NeuralNetwork_New
    (
        unsigned int            InputDimensionCount,
        size_t                  *InputDimensions,
        NeuralNetwork_t         **pNetwork
    );

NNStatus
    NeuralNetwork_AddLayer
    (
        NeuralNetwork_t         *Network,
        ActivationFn            ActivateFn,
        NeuronLayerInitFn       LayerInit,
        ...
    );

NNStatus
    NeuralNetwork_NewInstance
    (
        NeuralNetwork_t         *Network,
        NeuralInstance_t        **pInstance
    );

NNStatus
    NeuralInstance_Evaluate
    (
        NeuralInstance_t        *Instance,
        double                  *Inputs,
        double                  *Outputs
    );

void
    NeuralNetwork_Free
    (
        NeuralNetwork_t         *Network
    );
