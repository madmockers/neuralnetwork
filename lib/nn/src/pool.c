
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

#include <nn/aggregation.h>
#include <nn/activations.h>
#include <nn/neuron.h>
#include <nn/layer/pool.h>

#include "network_private.h"
#include "util.h"

typedef struct
{
    NeuronLayer_t           Layer;

    ActivationFn            Activate;

    PoolType_t              Type;

    unsigned int            DimensionCount;
    size_t                  FieldSize[ 0 ];
} Pool_t;

struct NeuronInitInfo
{
    Pool_t                  *Self;
    const NeuronGroup_t     *Inputs;
    ActivationFn            Activate;
    size_t                  *OutputDims;
};

static const NeuronLayerItf_t kLayerItf;

NNStatus
    LayerInit_Pool
    (
        NeuronLayer_t   **pLayer,
        NeuralNetwork_t *Network,
        ActivationFn    Activate,
        va_list         Args
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( pLayer  &&
        Network )
    {
        unsigned int dimCount = va_arg( Args, unsigned int );
        size_t       *dims    = va_arg( Args, size_t * );
        PoolType_t   type     = va_arg( Args, PoolType_t );

        if( dimCount > 0 )
        {
            Pool_t *self = malloc( sizeof( *self ) +
                                   sizeof( *dims ) * dimCount );

            if( self )
            {
                memset( self, '\0', sizeof( *self ) );

                self->Layer.Itf      = &kLayerItf;
                self->Layer.Network  = Network;

                self->DimensionCount = dimCount;
                memcpy( self->FieldSize, dims, sizeof( *dims ) * dimCount );
                self->Type           = type;

                *pLayer = &self->Layer;
                status  = NNSTATUS_OK;
            }
            else
            {
                status = NNSTATUS_ALLOC_FAILED;
            }
        }
        else
        {
            status = NNSTATUS_INVALID_ARGUMENT;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;    
}

static
double
    poolAggregate
    (
        NeuralInstance_t    *Instance,
        NeuronLayer_t       *Layer,
        Neuron_t            *Neuron,
        double              Initial
    )
{
    Pool_t *self = container_of( Layer, Pool_t, Layer );

    double selected = self->Type == POOL_TYPE_MAX ? DBL_MIN
                                                  : DBL_MAX;
    double *values  = Instance->Values;
    double *weights = Instance->Weights;

    for( size_t idx = 0
       ; idx < Neuron->InputCount
       ; idx++ )
    {
        double v = values[ Neuron->Inputs[ idx ]->ValueIdx ];

        if( ( self->Type == POOL_TYPE_MAX &&
              v > selected                ) ||
            ( self->Type == POOL_TYPE_MIN &&
              v < selected                ) )
        {
            selected = v;
        }
    }

    return Initial + selected;
}

static
NNStatus
    neuronInit
    (
        void                *Ctxt,
        size_t              Idx,
        Neuron_t            *Neuron
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Neuron &&
        Ctxt   )
    {
        struct NeuronInitInfo   *info           = Ctxt; 
        Pool_t                  *self           = info->Self;
        NeuralNetwork_t         *network        = self->Layer.Network;

        Neuron->Aggregate   = poolAggregate;
        Neuron->Activate    = info->Activate;

        size_t coord[ self->DimensionCount ];

        IdxToCoordinate( Idx,
                         self->DimensionCount,
                         info->OutputDims,
                         coord );
        CoordinateMul( self->DimensionCount,
                       self->FieldSize,
                       coord,
                       coord );

        size_t coordOffs[ self->DimensionCount ];
        memset( coordOffs, '\0', sizeof( size_t ) * self->DimensionCount );

        for( size_t inputIdx = 0 ; inputIdx < Neuron->InputCount ; inputIdx++ )
        {
            size_t inputCoords[ self->DimensionCount ];
            size_t idxOfInput;

            CoordinateAdd( self->DimensionCount,
                           coord,
                           coordOffs,
                           inputCoords ); 

            idxOfInput = CoordinateToIdx( info->Inputs->DimensionCount,
                                          info->Inputs->Dimensions,
                                          inputCoords );

            Neuron->Inputs[ inputIdx ] = info->Inputs->Neurons[ idxOfInput ];

            CoordinateIncrement( self->DimensionCount,
                                 self->FieldSize,
                                 coordOffs );
        }

        status              = NNSTATUS_OK;
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

static
NNStatus
    layerAllocate
    (
        NeuronLayer_t       *Layer,
        const NeuronGroup_t *Inputs
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Layer   &&
        Inputs  )
    {
        Pool_t *self = container_of( Layer, Pool_t, Layer );

        if( Inputs->DimensionCount == self->DimensionCount )
        {
            size_t          inputCount  = DimensionTotal( self->DimensionCount,
                                                          self->FieldSize );

            size_t          outputDims[ self->DimensionCount ];
            NeuronGroup_t   *outputs;

            for( size_t idx = 0 ; idx < self->DimensionCount ; idx++ )
            {
                outputDims[ idx ] = Inputs->Dimensions[ idx ] /
                                    self->FieldSize[ idx ];
            }

            struct NeuronInitInfo initInfo = {
                .Self       = self,
                .Inputs     = Inputs,
                .Activate   = self->Activate,
                .OutputDims = outputDims,
            };

            status = NeuronGroup_Allocate( self->Layer.Network,
                                           inputCount,
                                           self->DimensionCount,
                                           outputDims,
                                           &initInfo,
                                           neuronInit,
                                           &outputs );

            if( NNSTATUS_OK == status )
            {
                self->Layer.OutputNeurons = outputs;

                if( NNSTATUS_OK != status )
                {
                    NeuronGroup_Free( outputs, NULL, NULL );
                }
            }
            else
            {
                // status is set
            }
        }
        else
        {
            status = NNSTATUS_DIMENSION_MISMATCH;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

static
void
    layerDeallocate
    (
        NeuronLayer_t *Layer
    )
{
    if( Layer )
    {
        Pool_t *self = container_of( Layer, Pool_t, Layer );
        NeuronGroup_Free( self->Layer.OutputNeurons, NULL, NULL );
        free( self );
    }
}

static const NeuronLayerItf_t kLayerItf = {
    .Allocate           = layerAllocate,
    .Deallocate         = layerDeallocate,
};
