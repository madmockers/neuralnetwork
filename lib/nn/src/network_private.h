
#pragma once

#include <stdlib.h>

#include <nn/network.h>
#include <nn/neuron.h>
#include <nn/status.h>

typedef struct NeuralNetwork
{
    struct list_head    Layers;
    NeuronGroup_t       *Inputs;
    NeuronGroup_t       *Outputs;

    size_t              WeightCount;
    size_t              NeuronCount;
} NeuralNetwork_t;

static
inline
size_t
    NeuralNetwork_AllocateWeights
    (
        NeuralNetwork_t     *Network,
        size_t              Count
    )
{
    size_t idx = Network->WeightCount;
    Network->WeightCount += Count;
    return idx;
}
