
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include <nn/aggregation.h>
#include <nn/activations.h>
#include <nn/neuron.h>
#include <nn/layer/fullyconnected.h>

#include "network_private.h"
#include "util.h"

typedef struct
{
    NeuronLayer_t           Layer;

    size_t                  OutputCount;
    ActivationFn            Activate;
} FullyConnected_t;

struct NeuronInitInfo
{
    FullyConnected_t        *Self;
    const NeuronGroup_t     *Inputs;
    ActivationFn            Activate;
};

static const NeuronLayerItf_t kLayerItf;

NNStatus
    LayerInit_FullyConnected
    (
        NeuronLayer_t   **pLayer,
        NeuralNetwork_t *Network,
        ActivationFn    Activate,
        va_list         Args
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( pLayer  &&
        Network )
    {
        FullyConnected_t *self = malloc( sizeof( *self ) );

        if( self )
        {
            memset( self, '\0', sizeof( *self ) );

            self->Layer.Itf     = &kLayerItf;
            self->Layer.Network = Network;

            self->OutputCount   = va_arg( Args, size_t );
            self->Activate      = Activate;

            *pLayer = &self->Layer;
            status  = NNSTATUS_OK;
        }
        else
        {
            status = NNSTATUS_ALLOC_FAILED;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;    
}

static
NNStatus
    neuronInit
    (
        void                *Ctxt,
        size_t              Idx,
        Neuron_t            *Neuron
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Neuron &&
        Ctxt   )
    {
        struct NeuronInitInfo   *info           = Ctxt; 
        size_t                  weightsCount    = info->Inputs->TotalCount + 1;
        NeuralNetwork_t         *network        = info->Self->Layer.Network;

        Neuron->Aggregate   = Aggregate_Sum;
        Neuron->Activate    = info->Activate;
        Neuron->WeightsIdx  = NeuralNetwork_AllocateWeights( network, weightsCount );

        memcpy( Neuron->Inputs,
                info->Inputs->Neurons,
                sizeof( Neuron_t * ) * info->Inputs->TotalCount );

        status              = NNSTATUS_OK;
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

static
NNStatus
    layerAllocate
    (
        NeuronLayer_t       *Layer,
        const NeuronGroup_t *Inputs
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Layer   &&
        Inputs  )
    {
        FullyConnected_t *self = container_of( Layer, FullyConnected_t, Layer );

        size_t          inputCount  = Inputs->TotalCount;
        size_t          outputCount = self->OutputCount;
        NeuronGroup_t   *outputs;

        size_t dims[]           = { outputCount };

        struct NeuronInitInfo initInfo = {
            .Self               = self,
            .Inputs             = Inputs,
            .Activate           = self->Activate,
        };

        status = NeuronGroup_Allocate( self->Layer.Network,
                                       inputCount,
                                       1,
                                       dims,
                                       &initInfo,
                                       neuronInit,
                                       &outputs );

        if( NNSTATUS_OK == status )
        {
            self->Layer.OutputNeurons   = outputs;

            if( NNSTATUS_OK != status )
            {
                NeuronGroup_Free( outputs, NULL, NULL );
            }
        }
        else
        {
            // status is set
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

static
void
    layerDeallocate
    (
        NeuronLayer_t *Layer
    )
{
    if( Layer )
    {
        FullyConnected_t *self = container_of( Layer, FullyConnected_t, Layer );
        NeuronGroup_Free( self->Layer.OutputNeurons, NULL, NULL );
        free( self );
    }
}

static const NeuronLayerItf_t kLayerItf = {
    .Allocate           = layerAllocate,
    .Deallocate         = layerDeallocate,
};
