
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include <nn/layer.h>
#include <nn/list.h>
#include <nn/network.h>

#include "random.h"
#include "network_private.h"

NNStatus
    NeuralNetwork_New
    (
        unsigned int        InputDimensionCount,
        size_t              *InputDimensions,
        NeuralNetwork_t     **pNetwork
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( InputDimensions &&
        pNetwork        )
    {
        if( InputDimensionCount > 0 )
        {
            NeuralNetwork_t *net = malloc( sizeof( *net ) );

            if( net )
            {
                memset( net, '\0', sizeof( *net ) );

                NeuronGroup_t *inputs;

                status = NeuronGroup_Allocate( net,
                                               0,
                                               InputDimensionCount,
                                               InputDimensions,
                                               NULL,
                                               NULL,
                                               &inputs );

                if( NNSTATUS_OK == status )
                {
                    net->Inputs  = inputs;
                    net->Outputs = inputs;
                    INIT_LIST_HEAD( &net->Layers );
                    *pNetwork    = net;
                    status       = NNSTATUS_OK;
                }
                else
                {
                    // status is set
                }

                if( NNSTATUS_OK != status )
                {
                    free( net );
                }
            }
            else
            {
                status = NNSTATUS_ALLOC_FAILED;
            }
        }
        else
        {
            status = NNSTATUS_INVALID_ARGUMENT;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

NNStatus
    NeuralNetwork_AddLayer
    (
        NeuralNetwork_t         *Network,
        ActivationFn            Activate,
        NeuronLayerInitFn       LayerInit,
        ...
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Network   &&
        LayerInit )
    {
        NeuronLayer_t   *layer;
        va_list         ap;

        va_start( ap, LayerInit );
        status = LayerInit( &layer, Network, Activate, ap );
        va_end( ap );

        if( NNSTATUS_OK == status )
        {
            printf( "Layer inputs: %zu\n", Network->Outputs->TotalCount );
            status = layer->Itf->Allocate( layer, Network->Outputs );

            if( NNSTATUS_OK == status )
            {
                Network->Outputs = layer->OutputNeurons;
                list_add_tail( &layer->List, &Network->Layers );
            }
            else
            {
                // status is set
            }

            if( NNSTATUS_OK != status )
            {
                layer->Itf->Deallocate( layer );
            }
        }
        else
        {
            // status is set
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

void
    NeuralNetwork_Free
    (
        NeuralNetwork_t         *Network
    )
{
    if( Network )
    {
        NeuronLayer_t *layer, *j;

        list_for_each_entry_safe_reverse( layer, j, &Network->Layers, List )
        {
            list_del( &layer->List );
            layer->Itf->Deallocate( layer );
        }

        NeuronGroup_Free( Network->Inputs, NULL, NULL );
        free( Network );
    }
}

NNStatus
    NeuralNetwork_NewInstance
    (
        NeuralNetwork_t         *Network,
        NeuralInstance_t        **pInstance
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Network   &&
        pInstance )
    {
        NeuralInstance_t *instance;
        size_t           allocSize = sizeof( *instance ) +
                                     Network->WeightCount * sizeof( double ) +
                                     Network->NeuronCount * sizeof( double );

        instance = malloc( allocSize );

        if( instance )
        {
            memset( instance, '\0', sizeof( *instance ) );

            status = NN_RandomDoubleArray( Network->WeightCount + Network->NeuronCount,
                                           ( double * ) instance + 1 );

            if( NNSTATUS_OK == status )
            {
                instance->Network      = Network;
                instance->WeightCount  = Network->WeightCount;
                instance->Weights      = ( double * ) ( instance + 1 );
                instance->ValueCount   = Network->NeuronCount;
                instance->Values       = instance->Weights + Network->WeightCount;

                *pInstance = instance;
                status     = NNSTATUS_OK;
            }

            if( NNSTATUS_OK != status )
            {
                free( instance );
            }
        }
        else
        {
            status = NNSTATUS_ALLOC_FAILED;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

NNStatus
    NeuralInstance_Evaluate
    (
        NeuralInstance_t        *Instance,
        double                  *Inputs,
        double                  *Outputs
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Instance &&
        Inputs   &&
        Outputs  )
    {
        NeuralNetwork_t *network = Instance->Network;

        status = NeuronGroup_SetOutputs( network->Inputs,
                                         Instance,
                                         Inputs );

        if( NNSTATUS_OK == status )
        {
            NeuronLayer_t *layer;

            list_for_each_entry( layer, &network->Layers, List )
            {
                if( layer->Itf->PreEvaluate )
                {
                    layer->Itf->PreEvaluate( layer );
                }

                status = NNSTATUS_OK;
                for( size_t idx = 0 ; idx < layer->HiddenGroupCount &&
                                      NNSTATUS_OK != status         ; idx++ )
                {
                    status = NeuronGroup_Evaluate( layer->HiddenGroups[ idx ],
                                                   Instance,
                                                   layer );
                }

                if( NNSTATUS_OK == status )
                {
                    status = NeuronGroup_Evaluate( layer->OutputNeurons,
                                                   Instance,
                                                   layer );
                }

                if( NNSTATUS_OK != status )
                {
                    break;
                }
            }

            if( NNSTATUS_OK == status )
            {
                status = NeuronGroup_GetOutputs( network->Outputs,
                                                 Instance,
                                                 Outputs );
            }
            else
            {
                // status is set
            }
        }
        else
        {
            // status is set
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}
