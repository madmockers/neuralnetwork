
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>

#include <nn/status.h>

static int gDevUrandom = -1;

NNStatus
    NN_GetRandom
    (
        void        *Buffer,
        size_t      Size
    )
{
    if( -1 == gDevUrandom )
    {
        gDevUrandom = open( "/dev/urandom", O_RDONLY );
    }

    NNStatus status = NNSTATUS_UNSET;

    if( Buffer )
    {
        if( -1 != gDevUrandom )
        {
            size_t  inCount  = 0;
            uint8_t *byteBuf = Buffer;

            status = NNSTATUS_OK;
            while( inCount < Size        &&
                   NNSTATUS_OK == status )
            {
                ssize_t in = read( gDevUrandom, byteBuf + inCount, Size - inCount );

                if( in <= 0 )
                {
                    status = NNSTATUS_READ_FAILED;
                }
                else
                {
                    inCount += in;
                }
            }
        }
        else
        {
            status = NNSTATUS_OPEN_FAILED;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

NNStatus
    NN_RandomDoubleArray
    (
        size_t      Count,
        double      *Array
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Array )
    {
        status = NN_GetRandom( Array, sizeof( *Array ) * Count );

        if( NNSTATUS_OK == status )
        {
            for( size_t idx = 0 ; idx < Count ; idx++ )
            {
                double v = *( int * ) ( Array + idx );
                Array[ idx ] = v / ( double ) INT_MAX;
            }
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}
