
#pragma once

#include <stdlib.h>

#include <nn/status.h>

NNStatus
    NN_GetRandom
    (
        void        *Buffer,
        size_t      Size
    );

NNStatus
    NN_RandomDoubleArray
    (
        size_t      Count,
        double      *Array
    );
