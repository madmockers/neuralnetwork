
#include <stdlib.h>
#include <string.h>

#include <nn/status.h>
#include <nn/neuron.h>

#include "util.h"

NNStatus
    NeuronParams_AllocateArray
    (
        size_t          ParamCount,
        size_t          WeightCount,
        NeuronParams_t  ***pParams
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( pParams )
    {
        NeuronParams_t **params;
        size_t paramSize = sizeof( **params ) + sizeof( double ) * WeightCount;
        size_t allocSize =  (
                                sizeof( *params ) +
                                paramSize
                            ) * ParamCount;
        params = malloc( allocSize );

        if( params )
        {
            NeuronParams_t *paramPtr = ( NeuronParams_t * ) ( params + ParamCount );
            memset( paramPtr, '\0', paramSize * ParamCount );

            for( size_t idx = 0 ;
                 idx < ParamCount ;
                 idx++, paramPtr = ADDRESS_OFFSET( paramPtr, paramSize ) )
            {
                params[ idx ]           = paramPtr;
                paramPtr->InputCount    = WeightCount;
                paramPtr->Bias          = 1.0;
            }

            *pParams = params;
            status   = NNSTATUS_OK;
        }
        else
        {
            status = NNSTATUS_ALLOC_FAILED;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}
