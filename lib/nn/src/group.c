
#include <stdlib.h>
#include <string.h>

#include <nn/network.h>
#include <nn/neuron.h>
#include <nn/layer.h>

#include "network_private.h"
#include "util.h"

NNStatus
    NeuronGroup_Allocate
    (
        NeuralNetwork_t *Network,
        size_t          NeuronInputCount,
        unsigned int    DimensionCount,
        const size_t    *Dimensions,
        void            *InitCtxt,
        NeuronForEachFn InitFunction,
        NeuronGroup_t   **pAllocation
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( pAllocation )
    {
        size_t totalCount = DimensionTotal( DimensionCount, Dimensions );

        NeuronGroup_t *group;
        size_t baseGroupSize   = sizeof( *group ) + // base group
                                 sizeof( Neuron_t * ) * totalCount; // ptrs to neurons

        size_t neuronAllocSize = sizeof( Neuron_t ) +
                                 sizeof( Neuron_t * ) * NeuronInputCount;

        size_t allocSize = baseGroupSize +
                           sizeof( size_t ) * DimensionCount + // variable dimensions
                           neuronAllocSize * totalCount; // neurons themselves

        group = malloc( allocSize );

        if( group )
        {
            memset( group, '\0', allocSize ); 

            group->DimensionCount = DimensionCount;
            group->Dimensions     = ( size_t * ) ADDRESS_OFFSET( group, baseGroupSize );
            memcpy( group->Dimensions,
                    Dimensions,
                    sizeof( *Dimensions ) * DimensionCount );
            group->TotalCount     = totalCount;

            Neuron_t *nPtr = ( Neuron_t * ) ( group->Dimensions + DimensionCount );

            status = NNSTATUS_OK;
            for( size_t idx = 0 ; idx < totalCount      &&
                                  NNSTATUS_OK == status ; idx++ )
            {
                group->Neurons[ idx ] = nPtr;

                nPtr->InputCount = NeuronInputCount;
                nPtr->ValueIdx   = Network->NeuronCount++;
                if( InitFunction )
                {
                    status = InitFunction( InitCtxt,
                                           idx,
                                           nPtr );
                }
                nPtr = ADDRESS_OFFSET( nPtr, neuronAllocSize );
            }

            if( NNSTATUS_OK == status )
            {
                *pAllocation = group;
                status       = NNSTATUS_OK;
            }
        }
        else
        {
            status = NNSTATUS_ALLOC_FAILED;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

void
    NeuronGroup_Free
    (
        NeuronGroup_t   *Group,
        void            *FiniCtxt,
        NeuronForEachFn FiniFunction
    )
{
    if( Group )
    {
        if( FiniFunction )
        {
            NeuronGroup_ForEach( Group, FiniCtxt, FiniFunction );
        }
        free( Group );
    }
}

NNStatus
    NeuronGroup_ForEach
    (
        NeuronGroup_t   *Group,
        void            *Ctxt,
        NeuronForEachFn ForEach
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Group   &&
        ForEach )
    {
        status = NNSTATUS_OK;
        for( size_t idx = 0 ; idx < Group->TotalCount &&
                              NNSTATUS_OK == status   ; idx++ )
        {
            status = ForEach( Ctxt, idx, Group->Neurons[ idx ] );
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;    
}

NNStatus
    NeuronGroup_SetOutputs
    (
        NeuronGroup_t       *Group,
        NeuralInstance_t    *Instance,
        double              *Values
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Group   &&
        Values )
    {
        for( size_t idx = 0 ; idx < Group->TotalCount ; idx++ )
        {
            Instance->Values[ Group->Neurons[ idx ]->ValueIdx ] = Values[ idx ];
        }
        status = NNSTATUS_OK;
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

NNStatus
    NeuronGroup_GetOutputs
    (
        NeuronGroup_t       *Group,
        NeuralInstance_t    *Instance,
        double              *Values
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Group   &&
        Values )
    {
        for( size_t idx = 0 ; idx < Group->TotalCount ; idx++ )
        {
            Values[ idx ] = Instance->Values[ Group->Neurons[ idx ]->ValueIdx ];
        }
        status = NNSTATUS_OK;
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

struct EvalInfo
{
    NeuralInstance_t    *Instance;
    NeuronLayer_t       *Layer;
};

static
NNStatus
    forEachEvaluate
    (
        void            *Ctxt,
        size_t          Idx,
        Neuron_t        *Neuron
    )
{
    NNStatus        status  = NNSTATUS_UNSET;
    struct EvalInfo *info   = Ctxt;

    if( info   &&
        Neuron )
    {
        NeuralInstance_t *instance = info->Instance;

        double *weights  = instance->Weights;
        double *values   = instance->Values;

        size_t weightIdx = Neuron->WeightsIdx;
        double sum       = weights[ weightIdx++ ];

        if( Neuron->Aggregate == Aggregate_Sum )
        {
            for( size_t inputIdx = 0
               ; inputIdx < Neuron->InputCount
               ; inputIdx++,
                 weightIdx++ )
            {
                sum += values[ Neuron->Inputs[ inputIdx ]->ValueIdx ] *
                       weights[ weightIdx ];
            }
        }
        else
        {
            sum = Neuron->Aggregate( instance,
                                     info->Layer,
                                     Neuron,
                                     sum );
        }

        if( Neuron->Activate != Activation_Passthrough )
        {
            sum = Neuron->Activate( sum );
        }

        values[ Neuron->ValueIdx ] = sum;

        status = NNSTATUS_OK;
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

NNStatus
    NeuronGroup_Evaluate
    (
        NeuronGroup_t       *Group,
        NeuralInstance_t    *Instance,
        NeuronLayer_t       *Layer
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Group )
    {
        struct EvalInfo info = {
            .Instance = Instance,
            .Layer    = Layer
        };

        status = NeuronGroup_ForEach( Group, &info, forEachEvaluate );
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}
