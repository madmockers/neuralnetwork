
#include <math.h>

#include <nn/activations.h>

double
    Activation_Sigmoid
    (
        double      Value
    )
{
    return 1 / ( 1 + exp( -Value ) );
}

double
    Activation_ReLU
    (
        double      Value
    )
{
    return Value < 0 ? 0
                     : Value;
}
