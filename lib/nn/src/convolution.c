
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include <nn/aggregation.h>
#include <nn/activations.h>
#include <nn/neuron.h>
#include <nn/layer/convolution.h>

#include "network_private.h"
#include "util.h"

typedef struct
{
    NeuronLayer_t           Layer;

    ActivationFn            Activate;

    unsigned int            DimensionCount;
    size_t                  FieldSize[ 0 ];
} Convolution_t;

struct NeuronInitInfo
{
    Convolution_t           *Self;
    const NeuronGroup_t     *Inputs;
    ActivationFn            Activate;
    size_t                  Weights;
    size_t                  *OutputDims;
};

static const NeuronLayerItf_t kLayerItf;

NNStatus
    LayerInit_Convolution
    (
        NeuronLayer_t   **pLayer,
        NeuralNetwork_t *Network,
        ActivationFn    Activate,
        va_list         Args
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( pLayer  &&
        Network )
    {
        unsigned int dimCount = va_arg( Args, unsigned int );
        size_t       *dims    = va_arg( Args, size_t * );

        if( dimCount > 0 )
        {
            Convolution_t *self = malloc( sizeof( *self ) +
                                          sizeof( *dims ) * dimCount );

            if( self )
            {
                memset( self, '\0', sizeof( *self ) );

                self->Layer.Itf      = &kLayerItf;
                self->Layer.Network  = Network;

                self->DimensionCount = dimCount;
                memcpy( self->FieldSize, dims, sizeof( *dims ) * dimCount );

                *pLayer = &self->Layer;
                status  = NNSTATUS_OK;
            }
            else
            {
                status = NNSTATUS_ALLOC_FAILED;
            }
        }
        else
        {
            status = NNSTATUS_INVALID_ARGUMENT;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;    
}

static
NNStatus
    neuronInit
    (
        void                *Ctxt,
        size_t              Idx,
        Neuron_t            *Neuron
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Neuron &&
        Ctxt   )
    {
        struct NeuronInitInfo   *info           = Ctxt; 
        Convolution_t           *self           = info->Self;
        NeuralNetwork_t         *network        = self->Layer.Network;

        Neuron->Aggregate   = Aggregate_Sum;
        Neuron->Activate    = info->Activate;
        Neuron->WeightsIdx  = info->Weights;

        size_t coord[ self->DimensionCount ];

        IdxToCoordinate( Idx,
                         self->DimensionCount,
                         info->OutputDims,
                         coord );

        size_t coordOffs[ self->DimensionCount ];
        memset( coordOffs, '\0', sizeof( size_t ) * self->DimensionCount );

        for( size_t inputIdx = 0 ; inputIdx < Neuron->InputCount ; inputIdx++ )
        {
            size_t inputCoords[ self->DimensionCount ];
            size_t idxOfInput;

            CoordinateAdd( self->DimensionCount,
                           coord,
                           coordOffs,
                           inputCoords ); 

            //printf( "%zu, %zu\n", inputCoords[ 0 ], inputCoords[ 1 ] );
            idxOfInput = CoordinateToIdx( info->Inputs->DimensionCount,
                                          info->Inputs->Dimensions,
                                          inputCoords );
            //printf( "%zu\n", idxOfInput );
            //IdxToCoordinate( idxOfInput,
            //                 info->Inputs->DimensionCount,
            //                 info->Inputs->Dimensions,
            //                 inputCoords );
            //printf( "%zu, %zu\n", inputCoords[ 0 ], inputCoords[ 1 ] );

            Neuron->Inputs[ inputIdx ] = info->Inputs->Neurons[ idxOfInput ];

            CoordinateIncrement( self->DimensionCount,
                                 self->FieldSize,
                                 coordOffs );
        }

        status              = NNSTATUS_OK;
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

static
NNStatus
    layerAllocate
    (
        NeuronLayer_t       *Layer,
        const NeuronGroup_t *Inputs
    )
{
    NNStatus status = NNSTATUS_UNSET;

    if( Layer   &&
        Inputs  )
    {
        Convolution_t *self = container_of( Layer, Convolution_t, Layer );

        if( Inputs->DimensionCount == self->DimensionCount )
        {
            size_t          inputCount  = DimensionTotal( self->DimensionCount,
                                                          self->FieldSize );

            size_t          outputDims[ self->DimensionCount ];
            NeuronGroup_t   *outputs;

            for( size_t idx = 0 ; idx < self->DimensionCount ; idx++ )
            {
                outputDims[ idx ] = Inputs->Dimensions[ idx ] -
                                    self->FieldSize[ idx ] + 1;
            }

            struct NeuronInitInfo initInfo = {
                .Self       = self,
                .Inputs     = Inputs,
                .Activate   = self->Activate,
                .Weights    = NeuralNetwork_AllocateWeights( Layer->Network,
                                                             inputCount + 1 ),
                .OutputDims = outputDims,
            };

            status = NeuronGroup_Allocate( self->Layer.Network,
                                           inputCount,
                                           self->DimensionCount,
                                           outputDims,
                                           &initInfo,
                                           neuronInit,
                                           &outputs );

            if( NNSTATUS_OK == status )
            {
                self->Layer.OutputNeurons = outputs;

                if( NNSTATUS_OK != status )
                {
                    NeuronGroup_Free( outputs, NULL, NULL );
                }
            }
            else
            {
                // status is set
            }
        }
        else
        {
            status = NNSTATUS_DIMENSION_MISMATCH;
        }
    }
    else
    {
        status = NNSTATUS_NULL_ARGUMENT;
    }

    return status;
}

static
void
    layerDeallocate
    (
        NeuronLayer_t *Layer
    )
{
    if( Layer )
    {
        Convolution_t *self = container_of( Layer, Convolution_t, Layer );
        NeuronGroup_Free( self->Layer.OutputNeurons, NULL, NULL );
        free( self );
    }
}

static const NeuronLayerItf_t kLayerItf = {
    .Allocate           = layerAllocate,
    .Deallocate         = layerDeallocate,
};
