
#pragma once

#include <stdint.h>

#define ADDRESS_OFFSET( ptr, offset ) \
    ( ( typeof( ptr ) ) ( ( uintptr_t ) ( ptr ) + ( offset ) ) )

static inline
size_t
    DimensionTotal
    (
        unsigned int    DimensionCount,
        const size_t    *Dimensions
    )
{
    size_t total = 0;

    if( DimensionCount > 0 )
    {
        total = Dimensions[ 0 ];

        for( size_t idx = 1 ; idx < DimensionCount ; idx++ )
        {
            total *= Dimensions[ idx ];
        }
    }

    return total;
}

static inline
void
    CoordinateAdd
    (
        unsigned int    DimensionCount,
        const size_t    *CoordinateA,
        const size_t    *CoordinateB,
        size_t          *CoordinateOut
    )
{
    for( size_t dim = 0 ; dim < DimensionCount ; dim++ )
    {
        CoordinateOut[ dim ] = CoordinateA[ dim ] + CoordinateB[ dim ];
    }
}

static inline
void
    CoordinateMul
    (
        unsigned int    DimensionCount,
        const size_t    *CoordinateA,
        const size_t    *CoordinateB,
        size_t          *CoordinateOut
    )
{
    for( size_t dim = 0 ; dim < DimensionCount ; dim++ )
    {
        CoordinateOut[ dim ] = CoordinateA[ dim ] * CoordinateB[ dim ];
    }
}

static inline
void
    CoordinateIncrement
    (
        unsigned int    DimensionCount,
        const size_t    *Dimensions,
        size_t          *Coordinates
    )
{
    for( size_t dim = 0 ; dim < DimensionCount ; dim++ )
    {
        if( Coordinates[ dim ] + 1 < Dimensions[ dim ] )
        {
            Coordinates[ dim ]++;
            break;
        }
        else
        {
            Coordinates[ dim ] = 0;
        }
    }
}

static inline
void
    IdxToCoordinate
    (
        size_t          Idx,
        unsigned int    DimensionCount,
        const size_t    *Dimensions,
        size_t          *Coordinates
    )
{
    size_t mod = DimensionTotal( DimensionCount, Dimensions );
    for( size_t dim = DimensionCount ; dim > 0 ; dim-- )
    {
        mod /= Dimensions[ DimensionCount - dim ];
        if( 1 == mod )
        {
            Coordinates[ dim-1 ] = Idx;
        }
        else
        {
            Coordinates[ dim-1 ] = Idx % mod;
        }
        Idx /= Dimensions[ dim-1 ];
    }
}

static inline
size_t
    CoordinateToIdx
    (
        unsigned int    DimensionCount,
        const size_t    *Dimensions,
        const size_t    *Coordinates
    )
{
    size_t idx = 0;

    size_t mul = DimensionTotal( DimensionCount, Dimensions );
    for( size_t dim = 0 ; dim < DimensionCount ; dim++ )
    {
        mul /= Dimensions[ dim ];
        idx += Coordinates[ dim ] * mul;
    }

    return idx;
}
